package me.zhanghai.ohos.patternlock.sample.util;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import java.util.Set;

public class PreferenceUtils {
    private static DatabaseHelper databaseHelper = null;

    private PreferenceUtils() {}

    public static Preferences getPreferences(Context context){
        if (null == databaseHelper) {
            databaseHelper = new DatabaseHelper(context);
        }

        Preferences preferences = databaseHelper.getPreferences("patternlock");
        return preferences;
    }

    public static String getString(String key, String defaultValue, Context context){
        return getPreferences(context).getString(key, defaultValue);
    }

    public static Set<String> getStringSet(String key, Set<String> defaultValue, Context context) {
        return getPreferences(context).getStringSet(key, defaultValue);
    }

    public static int getInt(String key, int defaultValue, Context context) {
        return getPreferences(context).getInt(key, defaultValue);
    }

    public static long getLong(String key, long defaultValue, Context context) {
        return getPreferences(context).getLong(key, defaultValue);
    }

    public static float getFloat(String key, float defaultValue, Context context) {
        return getPreferences(context).getFloat(key, defaultValue);
    }

    public static boolean getBoolean(String key, boolean defaultValue, Context context) {
        return getPreferences(context).getBoolean(key, defaultValue);
    }

//    public static Preferences getEditor(Context context){
//        getPreferences(context).
//    }

    public static void putString(String key, String value, Context context){
        getPreferences(context).putString(key,value).flush();
    }

    public static void putStringSet(String key, Set<String> value, Context context){
        getPreferences(context).putStringSet(key, value).flush();
    }

    public static void putInt(String key, int value, Context context){
        getPreferences(context).putInt(key,value).flush();
    }

    public static void putLong(String key, long value, Context context) {
        getPreferences(context).putLong(key, value).flush();
    }

    public static void putFloat(String key, float value, Context context) {
        getPreferences(context).putFloat(key, value).flush();
    }

    public static void putBoolean(String key, boolean value, Context context) {
        getPreferences(context).putBoolean(key, value).flush();
    }

    public static void remove(String key, Context context) {
        getPreferences(context).delete(key).flush();
    }

    public static void clear(Context context) {
        getPreferences(context).clear().flush();
    }

}
