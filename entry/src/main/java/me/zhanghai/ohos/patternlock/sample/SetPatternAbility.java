package me.zhanghai.ohos.patternlock.sample;

import me.zhanghai.ohos.patternlock.sample.slice.SetPatternAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SetPatternAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SetPatternAbilitySlice.class.getName());
    }


}
