package me.zhanghai.ohos.patternlock.sample.slice;

import me.zhanghai.ohos.patternlock.PatternView;
import me.zhanghai.ohos.patternlock.sample.PatternLockAbility;
import me.zhanghai.ohos.patternlock.sample.ResourceTable;
import me.zhanghai.ohos.patternlock.sample.util.PatternLockUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

import java.util.List;

public class SetPatternAbilitySlice extends me.zhanghai.ohos.patternlock.SetPatternAbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        //super.setUIContent(ResourceTable.Layout_ability_set_pattern);
//        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.BLACK.getValue());
        Image back = (Image) findComponentById(ResourceTable.Id_setImage);
        back.setClickedListener(component -> PatternLockUtils.jumpAbility(getContext(), PatternLockAbility.class,getBundleName()));

    }

    @Override
    protected void onSetPattern(List<PatternView.Cell> pattern) {
        PatternLockUtils.setPattern(pattern, this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
