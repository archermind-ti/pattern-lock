package me.zhanghai.ohos.patternlock.sample;

import me.zhanghai.ohos.patternlock.sample.slice.AboutAbilitySlice;
import me.zhanghai.ohos.patternlock.sample.slice.MainAbilitySlice;
import me.zhanghai.ohos.patternlock.sample.slice.PatternLockAbilitySlice;
import me.zhanghai.ohos.patternlock.sample.util.PatternLockUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

    }

    public static final int RESULT_OK = -1;
    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
//        Text clearPattern = (Text) findComponentById(ResourceTable.Id_clear);
        super.onAbilityResult(requestCode, resultCode, resultData);
        if(resultData != null){
            int result = resultData.getIntParam("RESULT",0);
            if(result == RESULT_OK){
                PatternLockUtils.jumpAbility(getContext(),PatternLockAbility.class,getBundleName());
//                clearPattern.setTextColor(Color.BLACK);
//                clearPattern.setEnabled(true);

            }
        }

    }

}
