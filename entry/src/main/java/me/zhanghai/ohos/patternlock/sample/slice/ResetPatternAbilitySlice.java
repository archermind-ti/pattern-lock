package me.zhanghai.ohos.patternlock.sample.slice;

import me.zhanghai.ohos.patternlock.sample.ResourceTable;
import me.zhanghai.ohos.patternlock.sample.util.PatternLockUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class ResetPatternAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_reset);

        Button mCancelButton = (Button) findComponentById(ResourceTable.Id_cancel_button);
        Button mOkButton= (Button) findComponentById(ResourceTable.Id_ok_button);
        mOkButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                String msg = "";
                try {
                    msg = getResourceManager().getElement(ResourceTable.String_pattern_reset).getString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                PatternLockUtils.clearPattern(ResetPatternAbilitySlice.this);
                new ToastDialog(getContext())
                        .setText(msg)
                        .setAlignment(LayoutAlignment.CENTER)
                        .show();

                terminateAbility();
            }
        });
        mCancelButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
