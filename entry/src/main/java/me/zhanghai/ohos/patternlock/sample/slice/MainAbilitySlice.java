package me.zhanghai.ohos.patternlock.sample.slice;

import me.zhanghai.ohos.patternlock.sample.AboutAbility;
import me.zhanghai.ohos.patternlock.sample.ConfirmPatternAbility;
import me.zhanghai.ohos.patternlock.sample.PatternLockAbility;
import me.zhanghai.ohos.patternlock.sample.ResourceTable;
import me.zhanghai.ohos.patternlock.sample.util.PatternLockUtils;
import me.zhanghai.ohos.patternlock.sample.util.PreferenceContract;
import me.zhanghai.ohos.patternlock.sample.util.PreferenceUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.data.preferences.Preferences;

public class MainAbilitySlice extends AbilitySlice implements Preferences.PreferencesObserver{
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Text patternLock = (Text)findComponentById(ResourceTable.Id_pattern_lock);
        patternLock.setClickedListener(component -> {
            if (!PatternLockUtils.hasPattern(getContext())) {
                PatternLockUtils.jumpAbility(getContext(), PatternLockAbility.class,getBundleName());
            }
            else {
                Text clearPattern = (Text) findComponentById(ResourceTable.Id_clear);
                PatternLockUtils.jumpAbilityForResult(getAbility(), ConfirmPatternAbility.class,getBundleName());
            }

        });

        Text about = (Text)findComponentById(ResourceTable.Id_about);
        about.setClickedListener(component -> PatternLockUtils.jumpAbility(getContext(), AboutAbility.class,getBundleName()));

    }

    @Override
    public void onActive() {
        super.onActive();
        PreferenceUtils.getPreferences(this).registerObserver(this);
    }


    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        PreferenceUtils.getPreferences(this).unregisterObserver(this);
    }


    @Override
    public void onChange(Preferences preferences, String s) {
        if (s.equals(PreferenceContract.DEFAULT_THEME)) {
            //更换主题
        }
    }



}
