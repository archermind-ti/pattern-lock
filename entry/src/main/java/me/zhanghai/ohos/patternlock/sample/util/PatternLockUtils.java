package me.zhanghai.ohos.patternlock.sample.util;

import me.zhanghai.ohos.patternlock.PatternView;
import me.zhanghai.ohos.patternlock.sample.ConfirmPatternAbility;
import me.zhanghai.ohos.patternlock.sample.SetPatternAbility;
import me.zhanghai.ohos.patternlock.utils.PatternUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.utils.TextTool;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

import java.util.List;

public class PatternLockUtils {
    public static final int REQUEST_CODE_CONFIRM_PATTERN = 1214;

    private PatternLockUtils() {
    }

    public static void setPattern(List<PatternView.Cell> pattern, Context context) {
        PreferenceUtils.putString(PreferenceContract.KEY_PATTERN_SHA1,
                PatternUtils.patternToSha1String(pattern), context);
    }

    private static String getPatternSha1(Context context) {
        return PreferenceUtils.getString(PreferenceContract.KEY_PATTERN_SHA1,
                PreferenceContract.DEFAULT_PATTERN_SHA1, context);
    }

    public static boolean hasPattern(Context context) {
        return !TextTool.isNullOrEmpty(getPatternSha1(context));
    }

    public static boolean isPatternCorrect(List<PatternView.Cell> pattern, Context context) {
        return TextTool.isEqual(PatternUtils.patternToSha1String(pattern), getPatternSha1(context));
    }

    public static void clearPattern(Context context) {
        PreferenceUtils.remove(PreferenceContract.KEY_PATTERN_SHA1, context);
    }

    public static void setPatternByUser(Ability context) {
////        context.startAbility(new Intent(context, SetPatternViewAbilitySlice.class));
//        Intent intent = new Intent();
//        Operation operation = new Intent.OperationBuilder()
//                .withDeviceId("")
//                .withBundleName(getBundleName())
//                .withAbilityName(SetPatternViewAbility.class)
//                .build();
//
//// 把operation设置到intent中
//        intent.setOperation(operation);
//        context.startAbility(intent,2);
        jumpAbilityForResult(context, SetPatternAbility.class, context.getBundleName());
    }

    public static void confirmPattern(Ability activity, int requestCode) {
//        activity.startAbilityForResult(new Intent(activity, ConfirmPatternViewAbility.class),
//                requestCode);
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(activity.getBundleName())
                .withAbilityName(ConfirmPatternAbility.class)
                .build();

// 把operation设置到intent中
        intent.setOperation(operation);


        activity.startAbilityForResult(intent, requestCode);
    }

    public static void confirmPattern(Ability ability) {
        confirmPattern(ability, REQUEST_CODE_CONFIRM_PATTERN);
    }

    public static void confirmPatternIfHas(Ability ability) {
        if (hasPattern(ability.getContext())) {
            confirmPattern(ability);
        }
    }

    public static <ActivityType extends AbilitySlice & OnConfirmPatternResultListener> boolean
    checkConfirmPatternResult(ActivityType activity, int requestCode, int resultCode) {
        if (requestCode == REQUEST_CODE_CONFIRM_PATTERN) {
            activity.onConfirmPatternResult(resultCode == -1);
            return true;
        } else {
            return false;
        }
    }

    public interface OnConfirmPatternResultListener {
        void onConfirmPatternResult(boolean successful);
    }

    public static void jumpAbility(Context context, Class target, String bundleName) {
        Intent intent = new Intent();

        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(bundleName)
                .withAbilityName(target)
                .build();
        intent.setOperation(operation);
        context.startAbility(intent, 1);
    }


    public static void jumpAbilityForResult(Ability context, Class target, String bundleName) {
        Intent intent = new Intent();

        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(bundleName)
                .withAbilityName(target)
                .build();
        intent.setOperation(operation);
        context.startAbilityForResult(intent, 1);
    }

}
