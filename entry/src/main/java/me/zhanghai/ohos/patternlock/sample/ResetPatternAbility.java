package me.zhanghai.ohos.patternlock.sample;

import me.zhanghai.ohos.patternlock.sample.slice.ResetPatternAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ResetPatternAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ResetPatternAbilitySlice.class.getName());
    }
}
