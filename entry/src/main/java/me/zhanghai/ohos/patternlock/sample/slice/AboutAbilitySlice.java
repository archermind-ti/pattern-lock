package me.zhanghai.ohos.patternlock.sample.slice;

import me.zhanghai.ohos.patternlock.PatternView;

import me.zhanghai.ohos.patternlock.sample.MainAbility;
import me.zhanghai.ohos.patternlock.sample.ResourceTable;
import me.zhanghai.ohos.patternlock.sample.util.AppUtils;
import me.zhanghai.ohos.patternlock.sample.util.PatternLockUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.rpc.RemoteException;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class AboutAbilitySlice extends AbilitySlice {
    private static final List<PatternView.Cell> LOGO_PATTERN = new ArrayList<>();
    static {
        LOGO_PATTERN.add(PatternView.Cell.of(0, 1));
        LOGO_PATTERN.add(PatternView.Cell.of(1, 0));
        LOGO_PATTERN.add(PatternView.Cell.of(2, 1));
        LOGO_PATTERN.add(PatternView.Cell.of(1, 2));
        LOGO_PATTERN.add(PatternView.Cell.of(1, 1));
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_about);

        Image back = (Image) findComponentById(ResourceTable.Id_aboutImage);
        back.setClickedListener(component -> PatternLockUtils.jumpAbility(getContext(), MainAbility.class,getBundleName()));

        PatternView mPatternView = (PatternView) findComponentById(ResourceTable.Id_pattern_view);
        mPatternView.setPattern(PatternView.DisplayMode.Animate,LOGO_PATTERN);

        try {
            String version = getString(ResourceTable.String_about_version,
                    AppUtils.getPackageInfo(this).getVersionName());
            Text mVersionText = (Text) findComponentById(ResourceTable.Id_version_text);
//            mVersionText.setText(version);
            mVersionText.setText("版本2.1.2");
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        Text mGitHubText = (Text) findComponentById(ResourceTable.Id_github_text);
        mGitHubText.setClickedListener(component -> {
            Intent intent1 = new Intent();
            intent1.setAction("android.intent.action.VIEW");
            Uri uri = Uri.parse("https://github.com/zhanghai/PatternLock");
            intent1.setUri(uri);
            startAbility(intent1);
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
