package me.zhanghai.ohos.patternlock.sample.slice;

import me.zhanghai.ohos.patternlock.sample.MainAbility;
import me.zhanghai.ohos.patternlock.sample.ResourceTable;
import me.zhanghai.ohos.patternlock.sample.util.PatternLockUtils;
import me.zhanghai.ohos.patternlock.sample.util.PreferenceUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.WindowManager;
import ohos.data.DatabaseHelper;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

public class PatternLockAbilitySlice extends AbilitySlice {
    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "PatternLockAbilitySlice");

    private static final String KEY_CONFIRM_PATTERN_STARTED = "confirm_pattern_started";
    private static final String KEY_SHOULD_ADD_FRAGMENT = "should_add_fragment";

    private boolean mConfirmPatternStarted = false;
    private boolean mShouldAddFragment = false;
    private boolean mHasPattern = false;

    private CommonDialog mDialog;
    private Text text;
    private Text clearPattern;


    @Override
    public void onStart(Intent intent) {

        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_pattern_lock);
        Image back = (Image) findComponentById(ResourceTable.Id_lockImage);
        back.setClickedListener(component -> PatternLockUtils.jumpAbility(getContext(), MainAbility.class,getBundleName()));

//        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.BLACK.getValue());

//        WindowManager.getInstance().getTopWindow().get().addWindowFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);


        DirectionalLayout txtSetPattern = (DirectionalLayout) findComponentById(ResourceTable.Id_set_unlock_pattern);
        txtSetPattern.setClickedListener(component -> {

            System.out.println("Lock test: hasPattern = " + mHasPattern);
//                if (mHasPattern){
//                    PatternLockUtils.confirmPattern(getAbility());
////                    Text text = (Text) findComponentById(ResourceTable.Id_pref_key_set_pattern);
////                    text.setText("[已设置图案]");
//                }else {
            PatternLockUtils.setPatternByUser(getAbility());
//                }
        });
        text = (Text) findComponentById(ResourceTable.Id_pref_key_set_pattern);
        clearPattern = (Text) findComponentById(ResourceTable.Id_clear);

        if (PatternLockUtils.hasPattern(getContext())) {
            text.setText("[已设置图案]");
//

        } else {
            text.setText("[未设置图案]");
        }
//       boolean isHasPattern = PreferenceUtils.getBoolean("IS_HAS_PATTERN",false,getContext());
//        if (isHasPattern){
//            clearPattern = (Text) findComponentById(ResourceTable.Id_clear);
//            clearPattern.setEnabled(true);
//            clearPattern.setTextColor(Color.BLACK);
//        }else {
//            clearPattern = (Text) findComponentById(ResourceTable.Id_clear);
//            clearPattern.setEnabled(false);
//            try {
//                clearPattern.setTextColor(new Color(getResourceManager().getElement(ResourceTable.String_second_text_color).getColor()));
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (NotExistException e) {
//                e.printStackTrace();
//            } catch (WrongTypeException e) {
//                e.printStackTrace();
//            }
//        }

//        清除图案
        clearPattern.setClickedListener(component -> {
            DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_clear_pattern, null, false);
            Button button1 = (Button) toastLayout.findComponentById(ResourceTable.Id_buttn1);
            Button button2 = (Button) toastLayout.findComponentById(ResourceTable.Id_buttn2);
            button1.setClickedListener(component1 -> mDialog.remove());
            button2.setClickedListener(component12 -> {
                PatternLockUtils.clearPattern(getContext());
                clearPattern.setEnabled(false);
                text.setText("[未设置图案]");
                try {
                    clearPattern.setTextColor(new Color(getResourceManager().getElement(ResourceTable.String_second_text_color).getColor()));
                } catch (IOException | NotExistException | WrongTypeException e) {
                    e.printStackTrace();
                }
                mDialog.remove();

            });
            mDialog = new CommonDialog(getContext());
            mDialog.setContentCustomComponent(toastLayout);
            mDialog.setAutoClosable(true);
            mDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
            mDialog.setAlignment(LayoutAlignment.CENTER);
            mDialog.show();

        });


        Switch btnSwitch = (Switch) findComponentById(ResourceTable.Id_btn_switch);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadius(50);
        btnSwitch.setThumbElement(shapeElement);
        setSwitchState(btnSwitch);
        Text isDraw= (Text) findComponentById(ResourceTable.Id_isDraw);
        boolean checked = PreferenceUtils.getBoolean("KEY_LOCK_SWITCH_CHECKED", true, this);
        if (!checked){
            isDraw.setText("将在绘制时隐藏图案");
            btnSwitch.setChecked(false);
        }else {
            isDraw.setText("将在绘制时显示图案");
            btnSwitch.setChecked(true);
        }
        // 回调处理Switch状态改变事件
        btnSwitch.setCheckedStateChangedListener((button, isChecked) -> {
            PreferenceUtils.putBoolean("KEY_LOCK_SWITCH_CHECKED", isChecked, this);
            if (!isChecked){
                isDraw.setText("将在绘制时隐藏图案");
            }else {
                isDraw.setText("将在绘制时显示图案");
            }
        });
    }

    private void setSwitchState(Switch btnSwitch) {
        ShapeElement elementThumbOn = new ShapeElement();
//        elementThumbOn.setShape(ShapeElement.OVAL);
        elementThumbOn.setRgbColor(RgbColor.fromArgbInt(0xFF009688));
        elementThumbOn.setCornerRadius(50);
// 关闭状态下滑块的样式
        ShapeElement elementThumbOff = new ShapeElement();
//        elementThumbOff.setShape(ShapeElement.OVAL);
        elementThumbOff.setRgbColor(RgbColor.fromArgbInt(0xb3ffffff));
        elementThumbOff.setCornerRadius(50);
// 开启状态下轨迹样式
        ShapeElement elementTrackOn = new ShapeElement();
//        elementTrackOn.setShape(ShapeElement.RECTANGLE);
        elementTrackOn.setRgbColor(RgbColor.fromArgbInt(0xff888888));
        elementTrackOn.setCornerRadius(50);
// 关闭状态下轨迹样式
        ShapeElement elementTrackOff = new ShapeElement();
//        elementTrackOff.setShape(ShapeElement.RECTANGLE);
        elementTrackOff.setRgbColor(RgbColor.fromArgbInt(0xFF808080));
        elementTrackOff.setCornerRadius(50);

        btnSwitch.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
        btnSwitch.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));
    }

    private StateElement trackElementInit(ShapeElement on, ShapeElement off) {
        StateElement trackElement = new StateElement();
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return trackElement;
    }

    private StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
        StateElement thumbElement = new StateElement();
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return thumbElement;
    }

    @Override
    protected void onActive() {
        super.onActive();
        mHasPattern = PatternLockUtils.hasPattern(getContext());
        PreferenceUtils.putBoolean("IS_HAS_PATTERN",PatternLockUtils.hasPattern(getContext()),getContext());
        if (mHasPattern){
            clearPattern = (Text) findComponentById(ResourceTable.Id_clear);
            clearPattern.setEnabled(true);
            clearPattern.setTextColor(Color.BLACK);

        }
//        if (mHasPattern){
//            text.setText("[已设置图案]");
//            clearPattern.setEnabled(true);
//            clearPattern.setTextColor(Color.BLACK);
//        }else {
//            text.setText("[未设置图案]");
//            clearPattern.setEnabled(false);
//            try {
//                clearPattern.setTextColor(new Color(getResourceManager().getElement(ResourceTable.String_second_text_color).getColor()));
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (NotExistException e) {
//                e.printStackTrace();
//            } catch (WrongTypeException e) {
//                e.printStackTrace();
//            }
//        }
    }
}
