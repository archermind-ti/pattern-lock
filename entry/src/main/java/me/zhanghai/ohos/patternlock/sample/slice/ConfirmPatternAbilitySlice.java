package me.zhanghai.ohos.patternlock.sample.slice;

import me.zhanghai.ohos.patternlock.PatternView;
import me.zhanghai.ohos.patternlock.sample.MainAbility;
import me.zhanghai.ohos.patternlock.sample.ResetPatternAbility;
import me.zhanghai.ohos.patternlock.sample.ResourceTable;
import me.zhanghai.ohos.patternlock.sample.util.PatternLockUtils;
import me.zhanghai.ohos.patternlock.sample.util.PreferenceContract;
import me.zhanghai.ohos.patternlock.sample.util.PreferenceUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Switch;

import java.util.List;

public class ConfirmPatternAbilitySlice extends me.zhanghai.ohos.patternlock.ConfirmPatternAbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        Image back = (Image) findComponentById(ResourceTable.Id_setImage);
        back.setClickedListener(component -> PatternLockUtils.jumpAbility(getContext(), MainAbility.class,getBundleName()));

    }

    @Override
    protected boolean isStealthModeEnabled() {
        return !PreferenceUtils.getBoolean("KEY_LOCK_SWITCH_CHECKED", true, this);
//        return !PreferenceUtils.getBoolean(PreferenceContract.KEY_PATTERN_VISIBLE,
//                PreferenceContract.DEFAULT_PATTERN_VISIBLE, this);
    }

    @Override
    protected boolean isPatternCorrect(List<PatternView.Cell> pattern) {
        return PatternLockUtils.isPatternCorrect(pattern, this);
    }

    @Override
    protected void onForgotPassword() {
        PatternLockUtils.jumpAbility(getContext(), ResetPatternAbility.class,getBundleName());
        super.onForgotPassword();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
