package me.zhanghai.ohos.patternlock.sample.util;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.app.Context;
import ohos.bundle.BundleInfo;
import ohos.rpc.RemoteException;

public class AppUtils {
    private AppUtils() {}

    public static BundleInfo getPackageInfo(Context context) throws RemoteException {
        return context.getBundleManager().getBundleInfo(context.getBundleName(), 0);
    }

    public static void navigateUp(AbilitySlice activity, Intent extras){
        Intent upIntent = activity.getAbility().getIntent();
        if (upIntent != null) {
            if (extras != null) {
                IntentParams intentParams=new IntentParams();
                intentParams.setParam(extras.getBundle(),extras);
                upIntent.setParams(intentParams);
                activity.startAbility(upIntent);
            }else {
                upIntent.addFlags(Intent.FLAG_START_FOREGROUND_ABILITY);
                activity.startAbility(upIntent);
            }

        }
        activity.terminateAbility();
    }
    public static void navigateUp(AbilitySlice activity) {
        navigateUp(activity, null);
    }
    //给左上角的的左边加一个返回图标
    public static void setActionBarDisplayUp(AbilitySlice activity) {
    }
}
