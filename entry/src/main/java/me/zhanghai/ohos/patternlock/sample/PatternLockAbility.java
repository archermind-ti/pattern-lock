package me.zhanghai.ohos.patternlock.sample;

import me.zhanghai.ohos.patternlock.sample.slice.PatternLockAbilitySlice;
import me.zhanghai.ohos.patternlock.sample.util.PatternLockUtils;
import me.zhanghai.ohos.patternlock.sample.util.PreferenceUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class PatternLockAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PatternLockAbilitySlice.class.getName());
    }
    public static final int RESULT_OK = -1;
    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        Text clearPattern = (Text) findComponentById(ResourceTable.Id_clear);

        int dataResult=PreferenceUtils.getInt("RESULT_DATA",0,getContext());

        if(resultData != null){
            int result = resultData.getIntParam("RESULT",0);
            if(result == dataResult){
                Text text = (Text) findComponentById(ResourceTable.Id_pref_key_set_pattern);
                text.setText("[已设置图案]");
//                Text clearPattern = (Text) findComponentById(ResourceTable.Id_clear);
                clearPattern.setTextColor(Color.BLACK);
                clearPattern.setEnabled(true);
            }else {
                clearPattern.setEnabled(false);
                try {
                    clearPattern.setTextColor(new Color(getResourceManager().getElement(ResourceTable.String_second_text_color).getColor()));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                } catch (WrongTypeException e) {
                    e.printStackTrace();
                }
            }

        }




        PreferenceUtils.putInt("RESULT_DATA",RESULT_OK,getContext());
        if(resultData != null){
            int result = resultData.getIntParam("RESULT",0);
            if(result == RESULT_OK){
                Text text = (Text) findComponentById(ResourceTable.Id_pref_key_set_pattern);
                text.setText("[已设置图案]");
//                Text clearPattern = (Text) findComponentById(ResourceTable.Id_clear);
                clearPattern.setTextColor(Color.BLACK);
                clearPattern.setEnabled(true);
            }else {
                clearPattern.setEnabled(false);
                try {
                    clearPattern.setTextColor(new Color(getResourceManager().getElement(ResourceTable.String_second_text_color).getColor()));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                } catch (WrongTypeException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
