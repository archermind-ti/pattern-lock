package me.zhanghai.ohos.patternlock;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SetPatternAbilitySlice extends BasePatternAbilitySlice implements PatternView.OnPatternListener {

    private enum LeftButtonState{
        Cancel(ResourceTable.String_pl_cancel,true),
        CancelDisabled(ResourceTable.String_pl_cancel,false),
        Redraw(ResourceTable.String_pl_redraw,true),
        RedrawDisabled(ResourceTable.String_pl_redraw,false);

        public final int textId;
        public final boolean enabled;

        LeftButtonState(int textId, boolean enabled) {
            this.textId = textId;
            this.enabled = enabled;
        }
    }

    private enum RightButtonState {
        Continue(ResourceTable.String_pl_continue, true),
        ContinueDisabled(ResourceTable.String_pl_continue, false),
        Confirm(ResourceTable.String_pl_confirm, true),
        ConfirmDisabled(ResourceTable.String_pl_confirm, false);

        public final int textId;
        public final boolean enabled;

        RightButtonState(int textId, boolean enabled) {
            this.textId = textId;
            this.enabled = enabled;
        }
    }
    private enum Stage {

        Draw(ResourceTable.String_pl_draw_pattern, LeftButtonState.Cancel, RightButtonState.ContinueDisabled,
                true),
        DrawTooShort(ResourceTable.String_pl_pattern_too_short, LeftButtonState.Redraw,
                RightButtonState.ContinueDisabled, true),
        DrawValid(ResourceTable.String_pl_pattern_recorded, LeftButtonState.Redraw, RightButtonState.Continue,
                false),
        Confirm(ResourceTable.String_pl_confirm_pattern, LeftButtonState.Cancel,
                RightButtonState.ConfirmDisabled, true),
        ConfirmWrong(ResourceTable.String_pl_wrong_pattern, LeftButtonState.Cancel,
                RightButtonState.ConfirmDisabled, true),
        ConfirmCorrect(ResourceTable.String_pl_pattern_confirmed, LeftButtonState.Cancel,
                RightButtonState.Confirm, false);

        public final int messageId;
        public final LeftButtonState leftButtonState;
        public final RightButtonState rightButtonState;
        public final boolean patternEnabled;

        Stage(int messageId, LeftButtonState leftButtonState, RightButtonState rightButtonState,
              boolean patternEnabled) {
            this.messageId = messageId;
            this.leftButtonState = leftButtonState;
            this.rightButtonState = rightButtonState;
            this.patternEnabled = patternEnabled;
        }
    }

    private static final String KEY_STAGE = "stage";
    private static final String KEY_PATTERN = "pattern";

    private int mMinPatternSize;
    private List<PatternView.Cell> mPattern;
    private Stage mStage;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        mMinPatternSize = getMinPatternSize();
        mPatternView.setOnPatternListener(this);
        mLeftButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onLeftButtonClicked();
            }
        });
        mRightButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onRightButtonClicked();
            }
        });

        updateStage(Stage.Draw);

    }


    @Override
    public void onPatternStart() {
        removeClearPatternRunnable();

        mMessageText.setText(ResourceTable.String_pl_recording_pattern);
        mPatternView.setDisplayMode(PatternView.DisplayMode.Correct);
        mLeftButton.setEnabled(false);
        mRightButton.setEnabled(false);
    }

    @Override
    public void onPatternCleared() {
        removeClearPatternRunnable();
    }

    @Override
    public void onPatternCellAdded(List<PatternView.Cell> pattern) {
    }

    @Override
    public void onPatternDetected(List<PatternView.Cell> newPattern) {
        switch (mStage) {
            case Draw:
            case DrawTooShort:
                if (newPattern.size() < mMinPatternSize) {
                    updateStage(Stage.DrawTooShort);
                } else {
                    mPattern = new ArrayList<>(newPattern);
                    updateStage(Stage.DrawValid);
                }
                break;
            case Confirm:
            case ConfirmWrong:
                if (newPattern.equals(mPattern)) {
                    updateStage(Stage.ConfirmCorrect);
                } else {
                    updateStage(Stage.ConfirmWrong);
                }
                break;
            default:
                throw new IllegalStateException("Unexpected stage " + mStage + " when "
                        + "entering the pattern.");
        }
    }

    private void onLeftButtonClicked(){
        if (mStage.leftButtonState == LeftButtonState.Redraw) {
            mPattern = null;
            updateStage(Stage.Draw);
        } else if (mStage.leftButtonState == LeftButtonState.Cancel) {
            onCanceled();
        } else {
            throw new IllegalStateException("left footer button pressed, but stage of " + mStage
                    + " doesn't make sense");
        }
    }

    protected void onCanceled() {
        Intent intent=new Intent();
        intent.setParam("RESULT",0);
        setResult(intent);
        terminate();
    }

    private void onRightButtonClicked() {
        if (mStage.rightButtonState == RightButtonState.Continue) {
            if (mStage != Stage.DrawValid) {
                throw new IllegalStateException("expected ui stage " + Stage.DrawValid
                        + " when button is " + RightButtonState.Continue);
            }
            updateStage(Stage.Confirm);
        } else if (mStage.rightButtonState == RightButtonState.Confirm) {
            if (mStage != Stage.ConfirmCorrect) {
                throw new IllegalStateException("expected ui stage " + Stage.ConfirmCorrect
                        + " when button is " + RightButtonState.Confirm);
            }
            onSetPattern(mPattern);
            onConfirmed();
        }
    }

    protected void onConfirmed() {
        Intent intent=new Intent();
        intent.setParam("RESULT",-1);
        getAbility().setResult(-1,intent);
        terminate();
    }

    private void updateStage(Stage newStage) {
        int enableColor = 0;
        int disableColor = 0;
        try {
            enableColor = getResourceManager().getElement(ResourceTable.String_primary_text_default_light).getColor();
            disableColor = getResourceManager().getElement(ResourceTable.String_second_text_default_light).getColor();
        } catch (Exception e) {
            e.printStackTrace();
        }


        Stage previousStage = mStage;
        mStage = newStage;

        if (mStage == Stage.DrawTooShort) {
            mMessageText.setText(getString(mStage.messageId, mMinPatternSize));
        } else {
            mMessageText.setText(mStage.messageId);
        }

        mLeftButton.setText(mStage.leftButtonState.textId);
        mLeftButton.setEnabled(mStage.leftButtonState.enabled);
        if (mStage.leftButtonState.enabled) {
            mLeftButton.setTextColor(new Color(enableColor));
        } else {
            mLeftButton.setTextColor(new Color(disableColor));
        }

        mRightButton.setText(mStage.rightButtonState.textId);
        mRightButton.setEnabled(mStage.rightButtonState.enabled);
        if (mStage.rightButtonState.enabled) {
            mRightButton.setTextColor(new Color(enableColor));
        } else {
            mRightButton.setTextColor(new Color(disableColor));
        }

        mPatternView.setInputEnabled(mStage.patternEnabled);

        switch (mStage) {
            case Draw:
                // clearPattern() resets display mode to DisplayMode.Correct.
                mPatternView.clearPattern();
                break;
            case DrawTooShort:
                mPatternView.setDisplayMode(PatternView.DisplayMode.Wrong);
                postClearPatternRunnable();
                break;
            case Confirm:
                mPatternView.clearPattern();
                break;
            case ConfirmWrong:
                mPatternView.setDisplayMode(PatternView.DisplayMode.Wrong);
                postClearPatternRunnable();
                break;
            case DrawValid:
            case ConfirmCorrect:
                break;
        }

        // If the stage changed, announce the header for accessibility. This
        // is a no-op when accessibility is disabled.

    }

    protected int getMinPatternSize() {
        return 4;
    }

    protected void onSetPattern(List<PatternView.Cell> pattern) {}
}
