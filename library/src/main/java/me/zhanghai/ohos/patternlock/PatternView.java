package me.zhanghai.ohos.patternlock;


import me.zhanghai.ohos.patternlock.utils.AttrUtils;
import me.zhanghai.ohos.patternlock.utils.Rect;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;

import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

public class PatternView extends Component implements Component.DrawTask, Component.TouchEventListener {
    public static final int PATTERN_SIZE_DEFAULT = 3;

    // Aspect to use when rendering this view
    private static final int ASPECT_SQUARE = 0; // View will be the minimum of width/height
    private static final int ASPECT_LOCK_WIDTH = 1; // Fixed width; height will be minimum of (w,h)
    private static final int ASPECT_LOCK_HEIGHT = 2; // Fixed height; width will be minimum of (w,h)

    private Cell[][] mCells;

    private static final boolean PROFILE_DRAWING = false;
    private CellState[][] mCellStates;

    private final int mDotSize;
    private final int mDotSizeActivated;
    private final int mPathWidth;

    private boolean mDrawingProfilingStarted = false;

    private final Paint mPaint = new Paint();
    private final Paint mPathPaint = new Paint();

    /**
     * How many milliseconds we spend animating each circle of a lock pattern
     * if the animating mode is set.  The entire animation should take this
     * constant * the length of the pattern to complete.
     */
    private static final int MILLIS_PER_CIRCLE_ANIMATING = 700;

    /**
     * This can be used to avoid updating the display for very small motions or noisy panels.
     * It didn't seem to have much impact on the devices tested, so currently set to 0.
     */
    private static final float DRAG_THRESHOLD = 0.0f;
    public static final int VIRTUAL_BASE_VIEW_ID = 1;
    public static final boolean DEBUG_A11Y = false;
    private static final String TAG = "LockPatternView";

    private OnPatternListener mOnPatternListener;
    private ArrayList<Cell> mPattern;

    /**
     * Lookup table for the circles of the pattern we are currently drawing.
     * This will be the cells of the complete pattern unless we are animating,
     * in which case we use this to hold the cells we are drawing for the in
     * progress animation.
     */
    private boolean[][] mPatternDrawLookup;

    /**
     * the in progress point:
     * - during interaction: where the user's finger is
     * - during animation: the current tip of the animating line
     */
    private float mInProgressX = -1;
    private float mInProgressY = -1;

    private long mAnimatingPeriodStart;

    private DisplayMode mPatternDisplayMode = DisplayMode.Correct;
    private boolean mInputEnabled = true;
    private boolean mInStealthMode = false;
    private boolean mPatternInProgress = false;

    private float mHitFactor = 0.6f;

    private float mSquareWidth;
    private float mSquareHeight;
    private int screenHeight;
    private int screenWidth;


    private boolean hasDraw;
    private int diffY = -50;
    //      private int diffY = -300;
    private int diffX = -50;
    private int diffYMove = -260;
    //    private int diffYMove = -800;
    private int diffXMove = -50;

    private final Path mCurrentPath = new Path();
    private final Rect mInvalidate = new Rect();
    private final Rect mTmpInvalidateRect = new Rect();

    private int mRowCount;
    private int mColumnCount;
    private int mAspect;
    private int mRegularColor;
    private int mErrorColor;
    private int mSuccessColor;

    private static final String rowCount = "rowCount";
    private static final String columnCount = "columnCount";
    private static final String aspectRatio = "aspectRatio";
    private static final String pathWidth = "pathWidth";
    private static final String normalStateColor = "normalStateColor";
    private static final String correctStateColor = "correctStateColor";
    private static final String wrongStateColor = "wrongStateColor";
    private static final String dotSize = "dotSize";
    private static final String dotSizeActivated = "dotSizeActivated";

    private Context mContext;

    /**
     * Represents a cell in the matrix of the unlock pattern view.
     */
    public static final class Cell {

        final int row;
        final int column;

        /**
         * @param row    The row of the cell.
         * @param column The column of the cell.
         */
        private Cell(int row, int column) {
            this.row = row;
            this.column = column;
        }

        public int getRow() {
            return row;
        }

        public int getColumn() {
            return column;
        }

        public static Cell of(int row, int column) {
            return new Cell(row, column);
        }

        @Override
        public String toString() {
            return "(row=" + row + ",clmn=" + column + ")";
        }
    }

    public static class CellState {
        int row;
        int col;
        float radius;
        float translationY;
        float alpha = 1.0f;
        public float lineEndX = Float.MIN_VALUE;
        public float lineEndY = Float.MIN_VALUE;
        public AnimatorValue lineAnimator;
    }

    /**
     * How to display the current pattern.
     */
    public enum DisplayMode {

        /**
         * The pattern drawn is correct (i.e draw it in a friendly color)
         */
        Correct,

        /**
         * Animate the pattern (for demo, and help).
         */
        Animate,

        /**
         * The pattern is wrong (i.e draw a foreboding color)
         */
        Wrong
    }

    /**
     * The call back interface for detecting patterns entered by the user.
     */
    public interface OnPatternListener {

        /**
         * A new pattern has begun.
         */
        void onPatternStart();

        /**
         * The pattern was cleared.
         */
        void onPatternCleared();

        /**
         * The user extended the pattern currently being drawn by one cell.
         *
         * @param pattern The pattern with newly added cell.
         */
        void onPatternCellAdded(List<Cell> pattern);

        /**
         * A pattern was detected from the user.
         *
         * @param pattern The pattern.
         */
        void onPatternDetected(List<Cell> pattern);
    }

    public PatternView(Context context) {
        this(context, null);
    }

    public PatternView(Context context, AttrSet attrs) {
        super(context, attrs);

        mContext = context;

        mRowCount = AttrUtils.getIntFromAttr(attrs, rowCount, PATTERN_SIZE_DEFAULT);
        mColumnCount = AttrUtils.getIntFromAttr(attrs, columnCount, PATTERN_SIZE_DEFAULT);

        String aspect = AttrUtils.getStringFromAttr(attrs, aspectRatio, "square");
        if ("square".equals(aspect)) {
            mAspect = ASPECT_SQUARE;
        } else if ("lock_width".equals(aspect)) {
            mAspect = ASPECT_LOCK_WIDTH;
        } else if ("lock_height".equals(aspect)) {
            mAspect = ASPECT_LOCK_HEIGHT;
        } else {
            mAspect = ASPECT_SQUARE;
        }

        setClickable(true);

        mPathPaint.setAntiAlias(true);
        mPathPaint.setDither(true);

        mRegularColor = AttrUtils.getColorFromAttr(attrs, normalStateColor, Color.getIntColor("#000000"));
        mSuccessColor = AttrUtils.getColorFromAttr(attrs, correctStateColor, 0xff009688);
        mErrorColor = AttrUtils.getColorFromAttr(attrs, wrongStateColor, 0xffff0000);

        mPathPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPathPaint.setStrokeJoin(Paint.Join.ROUND_JOIN);
        mPathPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);

        mPathWidth = AttrUtils.getDimensionFromAttr(attrs, pathWidth, AttrHelper.vp2px(12, mContext));
        mPaint.setStrokeWidth(mPathWidth);

        mDotSize = AttrUtils.getDimensionFromAttr(attrs, dotSize, AttrHelper.vp2px(14, mContext));
        mDotSizeActivated = AttrUtils.getDimensionFromAttr(attrs, dotSizeActivated, 24);

        mPaint.setAntiAlias(true);
        mPaint.setDither(true);

        updatePatternSize();

        addDrawTask(this);
        setTouchEventListener(this);
    }

    public int getPatternRowCount() {
        return mRowCount;
    }

    public int getPatternColumnCount() {
        return mColumnCount;
    }

    public void setPatternSize(int rowCount, int columnCount) {
        if (mRowCount == rowCount && mColumnCount == columnCount) {
            return;
        }
        mRowCount = rowCount;
        mColumnCount = columnCount;
        updatePatternSize();
    }

    private void updatePatternSize() {
        mCells = new Cell[mRowCount][mColumnCount];
        for (int i = 0; i < mRowCount; i++) {
            for (int j = 0; j < mColumnCount; j++) {
                mCells[i][j] = Cell.of(i, j);
            }
        }
        mCellStates = new CellState[mRowCount][mColumnCount];
        for (int i = 0; i < mRowCount; i++) {
            for (int j = 0; j < mColumnCount; j++) {
                mCellStates[i][j] = new CellState();
                mCellStates[i][j].radius = mDotSize / 2f;
                mCellStates[i][j].row = i;
                mCellStates[i][j].col = j;
            }
        }
        mPattern = new ArrayList<>(mRowCount * mColumnCount);
        mPatternDrawLookup = new boolean[mRowCount][mColumnCount];
    }

    private void checkRange(int row, int column) {
        if (row < 0 || row >= mRowCount) {
            throw new IllegalArgumentException("row must be in range 0-" + (mRowCount - 1));
        }
        if (column < 0 || column >= mColumnCount) {
            throw new IllegalArgumentException("column must be in range 0-" + (mColumnCount - 1));
        }
    }

    public Cell getCellAt(int row, int column) {
        checkRange(row, column);
        return mCells[row][column];
    }

    public CellState[][] getCellStates() {
        return mCellStates;
    }

    /**
     * @return Whether the view is in stealth mode.
     */
    public boolean isInStealthMode() {
        return mInStealthMode;
    }

    /**
     * Set whether the view is in stealth mode.  If true, there will be no
     * visible feedback as the user enters the pattern.
     *
     * @param inStealthMode Whether in stealth mode.
     */
    public void setInStealthMode(boolean inStealthMode) {
        mInStealthMode = inStealthMode;
    }

    /**
     * Set the call back for pattern detection.
     *
     * @param onPatternListener The call back.
     */
    public void setOnPatternListener(
            OnPatternListener onPatternListener) {
        mOnPatternListener = onPatternListener;
    }

    /**
     * Set the pattern explicitly (rather than waiting for the user to input
     * a pattern).
     *
     * @param displayMode How to display the pattern.
     * @param pattern     The pattern.
     */
    public void setPattern(DisplayMode displayMode, List<Cell> pattern) {
        for (Cell cell : pattern) {
            checkRange(cell.getRow(), cell.getColumn());
        }

        mPattern.clear();
        mPattern.addAll(pattern);
        clearPatternDrawLookup();
        for (Cell cell : pattern) {
            mPatternDrawLookup[cell.getRow()][cell.getColumn()] = true;
        }

        setDisplayMode(displayMode);
    }

    /**
     * Get the display mode of the current pattern.
     */
    public DisplayMode getDisplayMode() {
        return mPatternDisplayMode;
    }

    /**
     * Set the display mode of the current pattern.  This can be useful, for
     * instance, after detecting a pattern to tell this view whether change the
     * in progress result to correct or wrong.
     *
     * @param displayMode The display mode.
     */
    public void setDisplayMode(DisplayMode displayMode) {
        mPatternDisplayMode = displayMode;
        if (displayMode == DisplayMode.Animate) {
            if (mPattern.size() == 0) {
                throw new IllegalStateException("you must have a pattern to "
                        + "animate if you want to set the display mode to animate");
            }
            mAnimatingPeriodStart = Time.getRealTime();
            final Cell first = mPattern.get(0);
            mInProgressX = getCenterXForColumn(first.getColumn());
            mInProgressY = getCenterYForRow(first.getRow());
            clearPatternDrawLookup();
        }
        invalidate();
    }

    public void startCellStateAnimation(CellState cellState, float startAlpha, float endAlpha,
                                        float startTranslationY, float endTranslationY,
                                        float startScale, float endScale, long delay, long duration,
                                        Runnable finishRunnable) {
        startCellStateAnimationSw(cellState, startAlpha, endAlpha, startTranslationY,
                endTranslationY, startScale, endScale, delay, duration,
                finishRunnable);
    }

    private void startCellStateAnimationSw(final CellState cellState, final float startAlpha,
                                           final float endAlpha, final float startTranslationY,
                                           final float endTranslationY, final float startScale,
                                           final float endScale, long delay, long duration,
                                           final Runnable finishRunnable) {
        cellState.alpha = startAlpha;
        cellState.translationY = startTranslationY;
        cellState.radius = mDotSize / 2f * startScale;
        AnimatorValue animator = new AnimatorValue();
        animator.setDuration(duration);
        animator.setDelay(delay);

        animator.setValueUpdateListener((animatorValue, t) -> {
            cellState.alpha = (1 - t) * startAlpha + t * endAlpha;
            cellState.translationY = (1 - t) * startTranslationY + t * endTranslationY;
            cellState.radius = mDotSize / 2f * ((1 - t) * startScale + t * endScale);
            invalidate();
        });

        animator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (finishRunnable != null) {
                    finishRunnable.run();
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        animator.start();
    }

    private void notifyCellAdded() {
        sendAccessEvent(ResourceTable.String_message_pattern_dot_added);
        if (mOnPatternListener != null) {
            mOnPatternListener.onPatternCellAdded(mPattern);
        }

    }

    private void notifyPatternStarted() {
        sendAccessEvent(ResourceTable.String_message_pattern_started);
        if (mOnPatternListener != null) {
            mOnPatternListener.onPatternStart();
        }
    }

    private void notifyPatternDetected() {
        sendAccessEvent(ResourceTable.String_message_pattern_detected);
        if (mOnPatternListener != null) {
            mOnPatternListener.onPatternDetected(mPattern);
        }
    }

    private void notifyPatternCleared() {
        sendAccessEvent(ResourceTable.String_message_pattern_cleared);
        if (mOnPatternListener != null) {
            mOnPatternListener.onPatternCleared();
        }
    }

    /**
     * Clear the pattern.
     */
    public void clearPattern() {
        resetPattern();
    }

    /**
     * Reset all pattern state.
     */
    private void resetPattern() {
        hasDraw = false;
        mPattern.clear();
        clearPatternDrawLookup();
        mPatternDisplayMode = DisplayMode.Correct;
        invalidate();
    }

    /**
     * Clear the pattern lookup table.
     */
    private void clearPatternDrawLookup() {
        for (int i = 0; i < mRowCount; i++) {
            for (int j = 0; j < mColumnCount; j++) {
                mPatternDrawLookup[i][j] = false;
            }
        }
    }

    /**
     * Enable or disable input. (for instance when displaying a message that will timeout so user
     * doesn't get view into messy state).
     */
    public void setInputEnabled(boolean enabled) {
        mInputEnabled = enabled;
    }


    /**
     * Determines whether the point x, y will add a new point to the current
     * pattern (in addition to finding the cell, also makes heuristic choices
     * such as filling in gaps based on current pattern).
     *
     * @param x The x coordinate.
     * @param y The y coordinate.
     */
    private Cell detectAndAddHit(float x, float y) {

        final Cell cell = checkForNewHit(x, y);
//        if (cell != null) {
//
//            // check for gaps in existing pattern
//            final ArrayList<Cell> pattern = mPattern;
//            if (!pattern.isEmpty()) {
//                final Cell lastCell = pattern.get(pattern.size() - 1);
//                int dRow = cell.row - lastCell.row;
//                int dColumn = cell.column - lastCell.column;
//
//                int dGcd = gcd(Math.abs(dRow), Math.abs(dColumn));
//                if (dGcd > 0) {
//                    int fillInRow = lastCell.row;
//                    int fillInColumn = lastCell.column;
//                    int fillInRowStep = dRow / dGcd;
//                    int fillInColumnStep = dColumn / dGcd;
//                    for (int i = 1; i < dGcd; ++i) {
//                        fillInRow += fillInRowStep;
//                        fillInColumn += fillInColumnStep;
//                        if (!mPatternDrawLookup[fillInRow][fillInColumn]) {
//                            Cell fillInGapCell = getCellAt(fillInRow, fillInColumn);
//                            addCellToPattern(fillInGapCell);
//                        }
//                    }
//                }
//            }
//
//            addCellToPattern(cell);
//            return cell;
//        }
        if (cell != null) {
            // Check for gaps in existing pattern
            Cell fillInGapDot = null;
            final ArrayList<Cell> pattern = mPattern;
            if (!pattern.isEmpty()) {
                Cell lastDot = pattern.get(pattern.size() - 1);
                int dRow = cell.row - lastDot.row;
                int dColumn = cell.column - lastDot.column;

                int fillInRow = lastDot.row;
                int fillInColumn = lastDot.column;

                if (Math.abs(dRow) == 2 && Math.abs(dColumn) != 1) {
                    fillInRow = lastDot.row + ((dRow > 0) ? 1 : -1);
                }

                if (Math.abs(dColumn) == 2 && Math.abs(dRow) != 1) {
                    fillInColumn = lastDot.column + ((dColumn > 0) ? 1 : -1);
                }

                fillInGapDot = Cell.of(fillInRow, fillInColumn);
            }

            if (fillInGapDot != null
                    && !mPatternDrawLookup[fillInGapDot.row][fillInGapDot.column]) {
                addCellToPattern(fillInGapDot);
            }
            addCellToPattern(cell);
            return cell;
        }

        return null;
    }

    // From
    // https://github.com/google/guava/blob/c462d69329709f72a17a64cb229d15e76e72199c/guava/src/com/google/common/math/IntMath.java

    /**
     * Returns the greatest common divisor of {@code a, b}. Returns {@code 0} if
     * {@code a == 0 && b == 0}.
     *
     * @throws IllegalArgumentException if {@code a < 0} or {@code b < 0}
     */
    private static int gcd(int a, int b) {

        /*
         * The reason we require both arguments to be >= 0 is because otherwise, what do you return
         * on gcd(0, Integer.MIN_VALUE)? BigInteger.gcd would return positive 2^31, but positive
         * 2^31 isn't an int.
         */
        //checkNonNegative("a", a);
        if (a < 0) {
            throw new IllegalArgumentException("a (" + a + ") must be >= 0");
        }
        //checkNonNegative("b", b);
        if (b < 0) {
            throw new IllegalArgumentException("b (" + b + ") must be >= 0");
        }

        if (a == 0) {
            // 0 % b == 0, so b divides a, but the converse doesn't hold.
            // BigInteger.gcd is consistent with this decision.
            return b;
        } else if (b == 0) {
            return a; // similar logic
        }
        /*
         * Uses the binary GCD algorithm; see http://en.wikipedia.org/wiki/Binary_GCD_algorithm.
         * This is >40% faster than the Euclidean algorithm in benchmarks.
         */
        int aTwos = Integer.numberOfTrailingZeros(a);
        a >>= aTwos; // divide out all 2s
        int bTwos = Integer.numberOfTrailingZeros(b);
        b >>= bTwos; // divide out all 2s
        while (a != b) { // both a, b are odd

            // The key to the binary GCD algorithm is as follows:
            // Both a and b are odd. Assume a > b; then gcd(a - b, b) = gcd(a, b).
            // But in gcd(a - b, b), a - b is even and b is odd, so we can divide out powers of two.

            // We bend over backwards to avoid branching, adapting a technique from
            // http://graphics.stanford.edu/~seander/bithacks.html#IntegerMinOrMax

            int delta = a - b; // can't overflow, since a and b are nonnegative

            int minDeltaOrZero = delta & (delta >> (Integer.SIZE - 1));
            // equivalent to Math.min(delta, 0)

            a = delta - minDeltaOrZero - minDeltaOrZero; // sets a to Math.abs(a - b)
            // a is now nonnegative and even

            b += minDeltaOrZero; // sets b to min(old a, b)
            a >>= Integer.numberOfTrailingZeros(a); // divide out all 2s, since 2 doesn't divide b
        }
        return a << Math.min(aTwos, bTwos);
    }

    private void addCellToPattern(Cell newCell) {
        mPatternDrawLookup[newCell.getRow()][newCell.getColumn()] = true;
        mPattern.add(newCell);
        if (!mInStealthMode) {
            startCellActivatedAnimation(newCell);
        }
        notifyCellAdded();
    }

    private void startCellActivatedAnimation(Cell cell) {
        final CellState cellState = mCellStates[cell.row][cell.column];
        startRadiusAnimation(mDotSize / 2f, mDotSizeActivated / 2f, 96,
                cellState, new Runnable() {
                    @Override
                    public void run() {
                        startRadiusAnimation(mDotSizeActivated / 2f, mDotSize / 2f, 192,
                                cellState, null);
                    }
                });
        startLineEndAnimation(cellState, mInProgressX, mInProgressY,
                getCenterXForColumn(cell.column), getCenterYForRow(cell.row));
    }

    private void startLineEndAnimation(final CellState state,
                                       final float startX, final float startY, final float targetX, final float targetY) {
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.CUBIC_BEZIER_ACCELERATION);
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float t) {
                state.lineEndX = (1 - t) * startX + t * targetX;
                state.lineEndY = (1 - t) * startY + t * targetY;
                invalidate();
            }
        });
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                state.lineAnimator = null;
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        animatorValue.setDuration(100);
        animatorValue.start();

        state.lineAnimator = animatorValue;
    }

    private void startRadiusAnimation(float start, float end, long duration,
                                      final CellState state,
                                      final Runnable endRunnable) {
        AnimatorValue animatorValue = new AnimatorValue();
        //animatorValue.setCurveType(Animator.CurveType.CUBIC_BEZIER_ACCELERATION);
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                //state.radius = v*(end-start);
                float tempSize = v * 100;
                if (v == 1) {
                    tempSize = mDotSize;
                }
                state.radius = tempSize / 2;
                invalidate();
            }
        });

        if (endRunnable != null) {
            animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    endRunnable.run();
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
        }

        animatorValue.setDuration(duration);
        animatorValue.start();

    }

    // helper method to find which cell a point maps to
    private Cell checkForNewHit(float x, float y) {

        final int rowHit = getRowHit(y);
        if (rowHit < 0) {
            return null;
        }
        final int columnHit = getColumnHit(x);
        if (columnHit < 0) {
            return null;
        }

        if (mPatternDrawLookup[rowHit][columnHit]) {
            return null;
        }
        return getCellAt(rowHit, columnHit);
    }

    /**
     * Helper method to find the row that y falls into.
     *
     * @param y The y coordinate
     * @return The row that y falls in, or -1 if it falls in no row.
     */
    private int getRowHit(float y) {

        final float squareHeight = mSquareHeight;
        float hitSize = squareHeight * mHitFactor;

        float offset = getPaddingTop() + (squareHeight - hitSize) / 2f;
        for (int i = 0; i < mRowCount; i++) {

            final float hitTop = offset + squareHeight * i;
            if (y >= hitTop && y <= hitTop + hitSize) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Helper method to find the column x fallis into.
     *
     * @param x The x coordinate.
     * @return The column that x falls in, or -1 if it falls in no column.
     */
    private int getColumnHit(float x) {
        final float squareWidth = mSquareWidth;
        float hitSize = squareWidth * mHitFactor;

        float offset = getPaddingLeft() + (squareWidth - hitSize) / 2f;
        for (int i = 0; i < mColumnCount; i++) {

            final float hitLeft = offset + squareWidth * i;
            if (x >= hitLeft && x <= hitLeft + hitSize) {
                return i;
            }
        }
        return -1;
    }


    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (!mInputEnabled || !isEnabled()) {
            return true;
        }

        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                handleActionDown(event);
                return true;
            case TouchEvent.PRIMARY_POINT_UP:
                handleActionUp();
                return true;
            case TouchEvent.POINT_MOVE:
                handleActionMove(event);
                return true;
            case TouchEvent.CANCEL:
            case TouchEvent.PHASE_CANCEL:
                if (mPatternInProgress) {
                    setPatternInProgress(false);
                    resetPattern();
                    notifyPatternCleared();
                }

                if (PROFILE_DRAWING) {
                    if (mDrawingProfilingStarted) {
                        mDrawingProfilingStarted = false;
                    }
                }
                return true;
        }
        return true;
    }

    private void setPatternInProgress(boolean progress) {
        mPatternInProgress = progress;
    }

    private void handleActionMove(TouchEvent event) {

        // Handle all recent motion events so we don't skip any cells even when the device
        // is busy...
        final float radius = mPathWidth;
        //final int historySize = event.getPointerCount();
        mTmpInvalidateRect.clear();
        boolean invalidateNow = false;
        //for (int i = 0; i < historySize; i++) {
        float x = event.getPointerPosition(0).getX();
        float y = event.getPointerPosition(0).getY();
        System.out.println("wzy:movey" + y);

        y = y + diffYMove;
        x = x + diffXMove;

        Cell hitCell = detectAndAddHit(x, y);
        final int patternSize = mPattern.size();
        if (hitCell != null && patternSize == 1) {
            setPatternInProgress(true);
            notifyPatternStarted();
        }
        // note current x and y for rubber banding of in progress patterns
        final float dx = Math.abs(x - mInProgressX);
        final float dy = Math.abs(y - mInProgressY);
        if (dx > DRAG_THRESHOLD || dy > DRAG_THRESHOLD) {
            invalidateNow = true;
        }

        if (mPatternInProgress && patternSize > 0) {
            final ArrayList<Cell> pattern = mPattern;
            final Cell lastCell = pattern.get(patternSize - 1);
            float lastCellCenterX = getCenterXForColumn(lastCell.column);
            float lastCellCenterY = getCenterYForRow(lastCell.row);

            // Adjust for drawn segment from last cell to (x,y). Radius accounts for line width.
            float left = Math.min(lastCellCenterX, x) - radius;
            float right = Math.max(lastCellCenterX, x) + radius;
            float top = Math.min(lastCellCenterY, y) - radius;
            float bottom = Math.max(lastCellCenterY, y) + radius;

            // Invalidate between the pattern's new cell and the pattern's previous cell
            if (hitCell != null) {
                final float width = mSquareWidth * 0.5f;
                final float height = mSquareHeight * 0.5f;
                final float hitCellCenterX = getCenterXForColumn(hitCell.column);
                final float hitCellCenterY = getCenterYForRow(hitCell.row);

                left = Math.min(hitCellCenterX - width, left);
                right = Math.max(hitCellCenterX + width, right);
                top = Math.min(hitCellCenterY - height, top);
                bottom = Math.max(hitCellCenterY + height, bottom);
            }

            // Invalidate between the pattern's last cell and the previous location
            mTmpInvalidateRect.union(Math.round(left), Math.round(top),
                    Math.round(right), Math.round(bottom));
        }
        //}
        mInProgressX = event.getPointerPosition(0).getX();
        mInProgressY = event.getPointerPosition(0).getY();
        mInProgressY = mInProgressY + diffYMove;
        mInProgressX = mInProgressX + diffXMove;

        // To save updates, we only invalidate if the user moved beyond a certain amount.
        if (invalidateNow) {
            mInvalidate.union(mTmpInvalidateRect);
            mInvalidate.set(mTmpInvalidateRect.left, mTmpInvalidateRect.top, mTmpInvalidateRect.right, mTmpInvalidateRect.bottom);
            invalidate();
        }
    }

    private void sendAccessEvent(int resId) {

    }

    private void handleActionUp() {
        // report pattern detected
        if (!mPattern.isEmpty()) {
            setPatternInProgress(false);
            //release();
            cancelLineAnimations();
            notifyPatternDetected();
            invalidate();
        }
        if (PROFILE_DRAWING) {
            if (mDrawingProfilingStarted) {
                mDrawingProfilingStarted = false;
            }
        }
    }

    private void cancelLineAnimations() {
        for (int i = 0; i < mRowCount; i++) {
            for (int j = 0; j < mColumnCount; j++) {
                CellState state = mCellStates[i][j];
                if (state.lineAnimator != null) {
                    state.lineAnimator.cancel();
                    state.lineEndX = Float.MIN_VALUE;
                    state.lineEndY = Float.MIN_VALUE;
                }
            }
        }
    }

    private void handleActionDown(TouchEvent event) {
        resetPattern();
        float x = event.getPointerPosition(0).getX();
        float y = event.getPointerPosition(0).getY();
        System.out.println("wzy:" + y);
        y = y + diffY - 185;
        x = x + diffX;

        final Cell hitCell = detectAndAddHit(x, y);
        if (hitCell != null) {
            setPatternInProgress(true);
            mPatternDisplayMode = DisplayMode.Correct;
            notifyPatternStarted();
        } else {//if (mPatternInProgress) {
            setPatternInProgress(false);
            notifyPatternCleared();
        }
        if (hitCell != null) {
//            float startX = getCenterXForColumn(hitCell.column);
//            float startY = getCenterYForRow(hitCell.row);
//
//            final float widthOffset = mSquareWidth / 2f;
//            final float heightOffset = mSquareHeight / 2f;

            invalidate();
        }
        mInProgressX = x;
        mInProgressY = y;
        if (PROFILE_DRAWING) {
            if (!mDrawingProfilingStarted) {
                mDrawingProfilingStarted = true;
            }
        }
    }


    private float getCenterXForColumn(int column) {
        return getPaddingLeft() + column * mSquareWidth + mSquareWidth / 2f;
    }

    private float getCenterYForRow(int row) {
        return getPaddingTop() + row * mSquareHeight + mSquareHeight / 2f;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

        if (!hasDraw) {
            screenWidth = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().width;
            screenHeight = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().height;
            int adjustedWidth = component.getWidth() - getPaddingLeft() - getPaddingRight();
            mSquareWidth = adjustedWidth / (float) mRowCount;
            int adjustedHeight = component.getHeight() - getPaddingTop() - getPaddingBottom();
            mSquareHeight = adjustedHeight / (float) mColumnCount;
        }

        final ArrayList<Cell> pattern = mPattern;
        final int count = pattern.size();
        final boolean[][] drawLookup = mPatternDrawLookup;

        if (mPatternDisplayMode == DisplayMode.Animate) {

            // figure out which circles to draw

            // + 1 so we pause on complete pattern
            final int oneCycle = (count + 1) * MILLIS_PER_CIRCLE_ANIMATING;
            final int spotInCycle = (int) (Time.getRealTime() -
                    mAnimatingPeriodStart) % oneCycle;
            final int numCircles = spotInCycle / MILLIS_PER_CIRCLE_ANIMATING;


            clearPatternDrawLookup();
            for (int i = 0; i < numCircles; i++) {
                final Cell cell = pattern.get(i);
                drawLookup[cell.getRow()][cell.getColumn()] = true;
            }

            // figure out in progress portion of ghosting line

            final boolean needToUpdateInProgressPoint = numCircles > 0
                    && numCircles < count;

            if (needToUpdateInProgressPoint) {

                final float percentageOfNextCircle =
                        ((float) (spotInCycle % MILLIS_PER_CIRCLE_ANIMATING)) /
                                MILLIS_PER_CIRCLE_ANIMATING;

                final Cell currentCell = pattern.get(numCircles - 1);
                final float centerX = getCenterXForColumn(currentCell.column);
                final float centerY = getCenterYForRow(currentCell.row);

                final Cell nextCell = pattern.get(numCircles);
                final float dx = percentageOfNextCircle *
                        (getCenterXForColumn(nextCell.column) - centerX);
                final float dy = percentageOfNextCircle *
                        (getCenterYForRow(nextCell.row) - centerY);
                mInProgressX = centerX + dx;
                mInProgressY = centerY + dy;
            }
            // TODO: Infinite loop here...

            //invalidate();
            TaskDispatcher taskDispatcher = mContext.getUITaskDispatcher();
            taskDispatcher.asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    invalidate();
                }
            });
        }

        final Path currentPath = mCurrentPath;
        currentPath.rewind();

        // draw the circles
        for (int i = 0; i < mRowCount; i++) {
            float centerY = getCenterYForRow(i);
            for (int j = 0; j < mColumnCount; j++) {
                CellState cellState = mCellStates[i][j];
                float centerX = getCenterXForColumn(j);
                float translationY = cellState.translationY;
                drawCircle(canvas, (int) centerX, (int) centerY + translationY, cellState.radius,
                        drawLookup[i][j], cellState.alpha);
            }
        }

        // TODO: the path should be created and cached every time we hit-detect a cell
        // only the last segment of the path should be computed here
        // draw the path of the pattern (unless we are in stealth mode)
        final boolean drawPath = !mInStealthMode;

        if (drawPath) {

            mPathPaint.setColor(new Color(getCurrentColor(true)));
            // Anyway other drawing sets their own alpha ignoring the original; And in this way we
            // can use ?colorControlNormal better.
            mPathPaint.setAlpha(255);

            boolean anyCircles = false;
            float lastX = 0f;
            float lastY = 0f;
            for (int i = 0; i < count; i++) {
                Cell cell = pattern.get(i);

                // only draw the part of the pattern stored in
                // the lookup table (this is only different in the case
                // of animation).
                if (!drawLookup[cell.row][cell.column]) {
                    break;
                }
                anyCircles = true;

                float centerX = getCenterXForColumn(cell.column);
                float centerY = getCenterYForRow(cell.row);
                if (i != 0) {
                    CellState state = mCellStates[cell.row][cell.column];
                    currentPath.rewind();
                    currentPath.moveTo(lastX, lastY);
                    if (Math.abs(state.lineEndX - Float.MIN_VALUE) > 0 && Math.abs(state.lineEndY - Float.MIN_VALUE) > 0) {
                        currentPath.lineTo(state.lineEndX, state.lineEndY);
                    } else {
                        currentPath.lineTo(centerX, centerY);
                    }

                    canvas.drawPath(currentPath, mPathPaint);
                }
                lastX = centerX;
                lastY = centerY;
            }

            // draw last in progress section
            if ((mPatternInProgress || mPatternDisplayMode == DisplayMode.Animate)
                    && anyCircles) {
                currentPath.rewind();
                currentPath.moveTo(lastX, lastY);
                currentPath.lineTo(mInProgressX, mInProgressY);

                mPathPaint.setAlpha((int) (calculateLastSegmentAlpha(
                        mInProgressX, mInProgressY, lastX, lastY) * 255f));
                canvas.drawPath(currentPath, mPathPaint);
            }
        }

        if (!hasDraw) {
            int occHeight = getHeight() + getMarginBottom();
            diffY = -(screenHeight - occHeight - 750 - 700);
            diffYMove = -(screenHeight - occHeight - 750 - 270);
            hasDraw = true;
        }

    }

    private float calculateLastSegmentAlpha(float x, float y, float lastX, float lastY) {
        float diffX = x - lastX;
        float diffY = y - lastY;
        float dist = (float) Math.sqrt(diffX * diffX + diffY * diffY);
        float frac = dist / mSquareWidth;
        return Math.min(1f, Math.max(0f, (frac - 0.3f) * 4f));
    }

    private int getCurrentColor(boolean partOfPattern) {
        if (!partOfPattern || mInStealthMode || mPatternInProgress) {
            // unselected circle
            return mRegularColor;
        } else if (mPatternDisplayMode == DisplayMode.Wrong) {
            // the pattern is wrong
            return mErrorColor;
        } else if (mPatternDisplayMode == DisplayMode.Correct ||
                mPatternDisplayMode == DisplayMode.Animate) {
            return mSuccessColor;
        } else {
            throw new IllegalStateException("unknown display mode " + mPatternDisplayMode);
        }
    }

    /**
     * @param partOfPattern Whether this circle is part of the pattern.
     */
    private void drawCircle(Canvas canvas, float centerX, float centerY, float radius,
                            boolean partOfPattern, float alpha) {
        mPaint.setColor(new Color(getCurrentColor(partOfPattern)));
        //mPaint.setColor(new Color(629131));
        mPaint.setAlpha((int) (alpha * 255));
        canvas.drawCircle(centerX, centerY, radius, mPaint);
    }


}
