package me.zhanghai.ohos.patternlock.utils;

import me.zhanghai.ohos.patternlock.PatternView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class PatternUtils {

    private PatternUtils() {}

    private static String bytesToString(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    private static byte[] stringToBytes(String string) {
        return Base64.getDecoder().decode(string);
    }

    public static byte[] patternToBytes(List<PatternView.Cell> pattern, int columnCount) {
        int patternSize = pattern.size();
        byte[] bytes = new byte[patternSize];
        for (int i = 0; i < patternSize; ++i) {
            PatternView.Cell cell = pattern.get(i);
            bytes[i] = (byte)(cell.getRow() * columnCount + cell.getColumn());
        }
        return bytes;
    }

    public static byte[] patternToBytes(List<PatternView.Cell> pattern) {
        return patternToBytes(pattern, 3);
    }

    public static List<PatternView.Cell> bytesToPattern(byte[] bytes, int columnCount) {
        List<PatternView.Cell> pattern = new ArrayList<>();
        for (byte b : bytes) {
            pattern.add(PatternView.Cell.of(b / columnCount, b % columnCount));
        }
        return pattern;
    }

    public static List<PatternView.Cell> bytesToPattern(byte[] bytes) {
        return bytesToPattern(bytes, 3);
    }

    public static String patternToString(List<PatternView.Cell> pattern, int columnCount) {
        return bytesToString(patternToBytes(pattern, columnCount));
    }

    public static String patternToString(List<PatternView.Cell> pattern) {
        //noinspection deprecation
        return patternToString(pattern, 3);
    }

    public static List<PatternView.Cell> stringToPattern(String string, int columnCount) {
        return bytesToPattern(stringToBytes(string), columnCount);
    }

    public static List<PatternView.Cell> stringToPattern(String string) {
        return stringToPattern(string,3);
    }

    private static byte[] sha1(byte[] input) {
        try {
            return MessageDigest.getInstance("SHA-1").digest(input);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] patternToSha1(List<PatternView.Cell> pattern, int columnCount) {
        return sha1(patternToBytes(pattern, columnCount));
    }

    public static byte[] patternToSha1(List<PatternView.Cell> pattern) {
        return patternToSha1(pattern, 3);
    }

    public static String patternToSha1String(List<PatternView.Cell> pattern, int columnCount) {
        return bytesToString(patternToSha1(pattern, columnCount));
    }

    public static String patternToSha1String(List<PatternView.Cell> pattern) {
        return patternToSha1String(pattern, 3);
    }

}
