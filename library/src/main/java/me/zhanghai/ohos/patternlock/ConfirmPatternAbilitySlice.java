package me.zhanghai.ohos.patternlock;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.util.List;

public class ConfirmPatternAbilitySlice extends BasePatternAbilitySlice implements PatternView.OnPatternListener {
    private static final String KEY_NUM_FAILED_ATTEMPTS = "num_failed_attempts";

    public static final int RESULT_FORGOT_PASSWORD = 1;
    public static final int RESULT_OK = -1;
    public static final int RESULT_CANCELED = 0;

    protected int mNumFailedAttempts;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        Text text = (Text) findComponentById(ResourceTable.Id_textDraw);
        text.setText("确认解锁图案");

        mMessageText.setText(ResourceTable.String_pl_draw_pattern_to_unlock);
        mPatternView.setInStealthMode(isStealthModeEnabled());
        mPatternView.setOnPatternListener(this);
        mLeftButton.setText(ResourceTable.String_pl_cancel);
        mLeftButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onCancel();
            }
        });
        mRightButton.setText(ResourceTable.String_pl_forgot_pattern);
        mRightButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onForgotPassword();
            }
        });
    }

    @Override
    public void onPatternStart() {
        removeClearPatternRunnable();

        // Set display mode to correct to ensure that pattern can be in stealth mode.
        mPatternView.setDisplayMode(PatternView.DisplayMode.Correct);
    }


    @Override
    public void onPatternCleared() {
        removeClearPatternRunnable();
    }

    @Override
    public void onPatternCellAdded(List<PatternView.Cell> pattern) {}

    @Override
    public void onPatternDetected(List<PatternView.Cell> pattern) {
        if (isPatternCorrect(pattern)) {
            onConfirmed();
        } else {
            mMessageText.setText(ResourceTable.String_pl_wrong_pattern);
            mPatternView.setDisplayMode(PatternView.DisplayMode.Wrong);
            postClearPatternRunnable();
            onWrongPattern();
        }
    }

    protected boolean isStealthModeEnabled() {
        return false;
    }

    protected boolean isPatternCorrect(List<PatternView.Cell> pattern) {
        return true;
    }

    protected void onConfirmed() {
        Intent intent=new Intent();
        intent.setParam("RESULT",RESULT_OK);
        getAbility().setResult(RESULT_OK,intent);
        terminate();
    }

    protected void onWrongPattern() {
        ++mNumFailedAttempts;
    }

    protected void onCancel() {
        Intent intent=new Intent();
        intent.setParam("RESULT",RESULT_CANCELED);
        setResult(intent);
        terminate();
    }

    protected void onForgotPassword() {
        Intent intent=new Intent();
        intent.setParam("RESULT",RESULT_FORGOT_PASSWORD);
        setResult(intent);
        terminate();
    }
}
