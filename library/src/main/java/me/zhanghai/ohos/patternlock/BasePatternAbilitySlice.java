package me.zhanghai.ohos.patternlock;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.Revocable;

public class BasePatternAbilitySlice extends AbilitySlice {
    private static final int CLEAR_PATTERN_DELAY_MILLI = 2000;

    protected Text mMessageText;
    protected PatternView mPatternView;
    protected DirectionalLayout mButtonContainer;
    protected Button mLeftButton;
    protected Button mRightButton;

    private final Runnable clearPatternRunnable = new Runnable() {
        public void run() {
            // clearPattern() resets display mode to DisplayMode.Correct.
            mPatternView.clearPattern();
        }
    };

    private final TaskDispatcher taskDispatcher = getUITaskDispatcher();
    private Revocable revocable = null;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_set_pattern);

        mMessageText=(Text)findComponentById(ResourceTable.Id_pl_message_text);
        mPatternView=(PatternView)findComponentById(ResourceTable.Id_pl_pattern);
        mButtonContainer=(DirectionalLayout)findComponentById(ResourceTable.Id_pl_button_container);
        mLeftButton=(Button)findComponentById(ResourceTable.Id_pl_left_button);
        mRightButton=(Button)findComponentById(ResourceTable.Id_pl_right_button);
    }

    protected void removeClearPatternRunnable(){
        if (null != revocable) {
            revocable.revoke();
        }
    }

    protected void postClearPatternRunnable() {
        TaskDispatcher dispatcher = getUITaskDispatcher();
        removeClearPatternRunnable();
        revocable = dispatcher.delayDispatch(new Runnable(){

            @Override
            public void run() {
                mPatternView.clearPattern();
            }
        }, CLEAR_PATTERN_DELAY_MILLI);
    }
}
